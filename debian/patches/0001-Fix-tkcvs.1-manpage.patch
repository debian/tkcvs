From: Boyuan Yang <byang@debian.org>
Date: Fri, 19 Apr 2024 21:40:17 -0400
Subject: Fix tkcvs.1 manpage

---
 tkcvs/tkcvs.1 | 28 ++++++++++++++--------------
 1 file changed, 14 insertions(+), 14 deletions(-)

diff --git a/tkcvs/tkcvs.1 b/tkcvs/tkcvs.1
index 50ec337..dacfa73 100644
--- a/tkcvs/tkcvs.1
+++ b/tkcvs/tkcvs.1
@@ -9,7 +9,7 @@ TkCVS - a Tk/Tcl Graphical Interface to CVS and Subversion
 TkCVS is a Tcl/Tk-based graphical interface to the CVS and Subversion configuration management systems. It displays the status of the files in the current working directory, and provides buttons and menus to execute configuration-management commands on the selected files. Limited RCS functionality is also present.  TkDiff is bundled in for browsing and merging your changes.
 .LP
 TkCVS also aids in browsing the repository. For Subversion, the repository tree is browsed like an ordinary file tree.  For CVS, the CVSROOT/modules file is read.  TkCVS extends CVS with a method to produce a browsable, "user friendly" listing of modules. This requires special comments in the CVSROOT/modules file. See "CVS Modules File" for more guidance.
-.SP
+.sp
 .SH OPTIONS
 .LP
 TkCVS accepts the following options.
@@ -37,7 +37,7 @@ Browse the modules located in CVSROOT /jaz/repository
 % tkcvs -log tstheap.c
 .RE
 View the log of the file tstheap.c
-.SP
+.sp
 .SH Working Directory Browser
 .LP
 The working directory browser shows the files in your local working copy, or "sandbox."  It shows the status of the files at a glance and provides tools to help with most of the common CVS, SVN, and RCS operations you might do.
@@ -195,7 +195,7 @@ This button resets the edit flag on the selected files, enabling other developer
 .TP
 .I Close:
 Press this button to close the Working Directory Browser. If no other windows are open, TkCVS exits.
-.SP
+.sp
 .SH Log (Branch) Browser
 .LP
 The TkCVS Log Browser window enables you to view a graphical display of the revision log of a file, including all previous versions and any branched versions.
@@ -234,7 +234,7 @@ The following special characters are used in the search pattern:
 .LP
   [chars] Matches any character in the set given by chars. If a sequence of the form x-y appears in chars, then any character between x and y, inclusive, will match.
 .LP
-  \x      Matches the single character x. This provides a way of avoiding the special interpretation of the characters *?[]\ in pattern.
+  \\x      Matches the single character x. This provides a way of avoiding the special interpretation of the characters *?[]\ in pattern.
 .LP
 If you only enter "FOO" (without the \") in the entry box, it searches the exact string "foo". If you want to search all strings starting with "foo", you have to put "foo*". For all strings containing "foo", you must put "*foo*".
 .SS Log Browser Buttons
@@ -263,7 +263,7 @@ This button lists all the tags applied to the file in a searchable text window.
 This button closes the Log Browser. If no other windows are open, TkCVS exits.
 .SS The View Options Menu
 The View Menu allows you to control what you see in the branch diagram.  You can choose how much information to show in the boxes, whether to show empty revisions, and whether to show tags.  You can even control the size of the boxes.  If you are using Subversion, you may wish to turn the display of tags off.  If they aren't asked for they won't be read from the repository, which can save a lot of time.
-.SP
+.sp
 .SH Merge Tool for CVS
 .LP
 The Merge Tool chooses a "representative" file in the current directory and diagrams the branch tags. It tries to pick the "bushiest" file, or failing that, the most-revised file. If you disagree with its choice, you can type the name of another file in the top entry and press Return to diagram that file instead.
@@ -275,7 +275,7 @@ The changes made on the branch since its beginning will be merged into the curre
 .TP
 .I Merge Changes to Current:
 Instead of merging from the base of the branch, this button merges the changes that were made since a particular version on the branch. It pops up a dialog in which you fill in the version. It should usually be the version that was last merged.
-.SP
+.sp
 .SH Module Browser
 .LP
 Operations that are performed on the repository instead of in a checked-out working directory are done with the Module Browser.  The most common of these operations is checking out or exporting from the repository.  The Module Browser can be started from the command line (tkcvs -win module) or started from the main window by pressing the big button.
@@ -329,7 +329,7 @@ This item creates a Larry Wall format patch(1) file of the module selected.
 .TP
 .I Close:
 This button closes the Repository Browser. If no other windows are open, TkCVS exits.
-.SP
+.sp
 .SH Importing New Modules
 .LP
 Before importing a new module, first check to make sure that you have write permission to the repository. Also you'll have to make sure the module name is not already in use.
@@ -365,7 +365,7 @@ A one-line descriptive title for your module.  This will be displayed in the rig
 The current version number of the module. This should be a number of the form X.Y.Z where .Y and .Z are optional. You can leave this blank, in which case 1 will be used as the first version number.
 .LP
 Importing a directory into Subversion is similar but not so complicated.  You use the SVN -> Import CWD into Repository menu.  You need supply only the path in the repository where you want the directory to go.  The repository must be prepared and the path must exist, however.
-.SP
+.sp
 .SH Importing to an Existing Module (CVS)
 .LP
 Before importing to an existing module, first check to make sure that you have write permission to the repository.
@@ -392,7 +392,7 @@ The location in the repository tree where the existing module is. Filled in by t
 .TP
 .I Version Number:
 The current version number of the module to be imported. This should be a number of the form X.Y.Z where .Y and .Z are optional. You can leave this blank, in which case 1 will be used as the first version number.
-.SP
+.sp
 .SH Vendor Merge (CVS)
 .LP
 Software development is sometimes based on source distribution from a vendor or third-party distributor. After building a local version of this distribution, merging or tracking the vendor's future release into the local version of the distribution can be done with the vendor merge command.
@@ -414,7 +414,7 @@ Ok the dialog. Several things happens now. Several screens will appear showing t
 The checked out local code will now contain changes from a merge between two revisions of the vendor modules. This code will not be checked into the repository. You can do that after you've reconciled conflicts and decide if that is what you really want. 
 .LP
 A detailed example on how to use the vendor merge operation is provided in the PDF file vendor5readme.pdf. 
-.SP
+.sp
 .SH Configuration Files
 .LP
 There are two configuration files for TkCVS. The first is stored in the directory in which the *.tcl files for TkCVS are installed. This is called tkcvs_def.tcl. You can put a file called site_def in that directory, too. That's a good place for site-specific things like tagcolours. Unlike tkcvs_def.tcl, it will not be overwritten when you install a newer version of TkCVS.
@@ -594,13 +594,13 @@ For debugging: C=CVS commands, E=CVS stderr output, F=File creation/deletion, T=
 .TP
 .B cvscfg(logging)
 Logging (debugging) on or off
-.SP
+.sp
 .SH Environment Variables
 .LP
 You should have the CVSROOT environment variable pointing to the location of your CVS repository before you run TkCVS. It will still allow you to work with different repositories within the same session.
 .LP
 If you wish TkCVS to point to a Subversion repository by default, you can set the environment variable SVNROOT.  This has no meaning to Subversion itself, but it will clue TkCVS if it's started in an un-versioned directory.
-.SP
+.sp
 .SH User Configurable Menu Extensions
 .LP
 It is possible to extend the TkCVS menu by inserting additional commands into the .tkcvs or tkcvs_def.tcl files. These extensions appear on an extra menu to the right of the TkCVS Options menu.
@@ -630,7 +630,7 @@ Causes a new menu option titled "view" to be added to the User defined menu that
 Any user-defined commands will be passed a list of file names corresponding to the files selected on the directory listing on the main menu as arguments.
 .LP
 The output of the user defined commands will be displayed in a window when the command is finished.
-.SP
+.sp
 .SH CVS Modules File
 .LP
 If you haven't put anything in your CVSROOT/modules file, please do so. See the "Administrative Files" section of the CVS manual. Then, you can add comments which TkCVS can use to title the modules and to display them in a tree structure.
@@ -700,7 +700,7 @@ When you are installing TkCVS, you may like to add these additional lines to the
 These extension lines commence with a "#" character, so CVS interprets them as comments. They can be safely left in the file whether you are using TkCVS or not.
 .LP
 "#M" is equivalent to "#D". The two had different functions in previous versions of TkCVS, but now both are parsed the same way.
-.SP
+.sp
 .SH SEE ALSO
 cvs(1), svn(1)
 .SH AUTHOR
